public class Camisa {
    private String talla;
    private String model;
    private String color;

    @Override
    public String toString() {
        return "Camisa{" +
                "talla='" + talla + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                '}';
    }

    public Camisa(String talla, String model, String color) {
        this.talla = talla;
        this.model = model;
        this.color = color;
    }

    public String getTalla() {
        return talla;
    }

    public void setTalla(String talla) {
        this.talla = talla;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
