import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Camisa> llistaCamises = new ArrayList<Camisa>();
        ArrayList<Camisa> llistaFinal = new ArrayList<Camisa>();

        llistaCamises.add(new Camisa("XL", "xDD", "verd"));
        llistaCamises.add(new Camisa("XXL", "xDD2", "vermell"));
        llistaCamises.add(new Camisa("M", "xDD3", "blau"));
        llistaCamises.add(new Camisa("XS", "xDD4", "groc"));
        llistaCamises.add(new Camisa("XD", "xDD5", "verd"));


        llistaFinal = filtrarCamises(llistaCamises, (Camisa c)-> c.getTalla().equals("XL"));
        System.out.println("Les camises de la talla XL són: ");
        for (Camisa c : llistaFinal)
        {
            System.out.println(c);
        }

        llistaFinal.clear();
        llistaFinal = filtrarCamises(llistaCamises, (Camisa c)-> c.getColor().equals("vermell"));
        System.out.println("Les camises de color vermell són: ");
        for (Camisa c : llistaFinal)
        {
            System.out.println(c);
        }

        llistaFinal.clear();
        llistaFinal = filtrarCamises(llistaCamises, (Camisa c)-> c.getTalla().equals("M") && c.getColor().equals("blau"));
        System.out.println("Les camises de talla M i color blau són: ");
        for (Camisa c : llistaFinal)
        {
            System.out.println(c);
        }
    }


    private static ArrayList<Camisa> filtrarCamises(ArrayList<Camisa> camises, CaracteristicaCamisa cC)
    {
        ArrayList<Camisa> llistaCamises = new ArrayList<Camisa>();
        for (Camisa c: camises)
        {
            if(cC.test(c))
            {
                llistaCamises.add(c);
            }
        }
        return llistaCamises;
    }

    @FunctionalInterface
    interface CaracteristicaCamisa{
        boolean test(Camisa c);
    }
}
