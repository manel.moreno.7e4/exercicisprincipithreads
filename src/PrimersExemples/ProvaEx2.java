package PrimersExemples;

public class ProvaEx2 {
    public static void main(String[] args) {
        Thread t1 = new Thread(new ThreadLong(60));
        Thread t2 = new Thread(new ThreadLong(100));
        Thread t3 = new Thread(new ThreadLong(160));
        t1.start();
        t2.start();
        t3.start();
    }

    private void senseRunnable()
    {
        ThreadLong t1 = new ThreadLong(10);
        ThreadLong t2 = new ThreadLong(20);
        ThreadLong t3 = new ThreadLong(30);
        //t1.start();
        //t2.start();
        //t3.start();
    }
}
