package PrimersExemples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ThreadFitxer implements Runnable{

    private String nom;

    public ThreadFitxer(String nom) {
        this.nom = nom;
    }


    @Override
    public void run() {
        System.out.println('\n'+nom+" està comptant..."+Thread.currentThread().getName());
        countLines();
    }

    private void countLines()
    {
        int count = 0;
        File file = new File("C:/"+nom+".txt");
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    file));
            String line = reader.readLine();
            while (line != null) {
                count++;
                line = reader.readLine();
            }
            reader.close();
            System.out.println("There are "+count+" lines in the document "+file.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
