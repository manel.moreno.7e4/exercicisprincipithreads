package PrimersExemples;

public class ThreadLong implements Runnable{
    private long num;

    public ThreadLong(long num) {
        this.num = num;
    }

    @Override
    public void run() {
        long result = num + 1;
        while(!IsPrimeNumber(result))
        {
            result++;
        }
        System.out.println("L' enter més proper següent a "+num+" és: "+result);
    }

    private boolean IsPrimeNumber(long num)
    {
        for (long i = num -1; i > 1; i--)
        {
            if(num % i == 0)
            {
                return false;
            }
        }
        return true;
    }
}
